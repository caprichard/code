#Broadcasting Terminal

import serial
import time

ID_ = '1'
ID_in = ID_ + '@#'
ID_out = '#@' + ID_

ser = serial.Serial(
        port='/dev/ttyS0',
        baudrate=9600,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS,
        timeout=1
)

startTime = time.time()

while True:
	sendTo = input("Enter the ID Number to the person you want to speak to. : ")
	sendTo = sendTo + '@#'
	msg = input("Send message here: ")
	msg = sendTo + msg + ID_out
	ser.write(str.encode(msg)))