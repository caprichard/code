#Recieving Terminal

import serial   #
import time     #IMPORT NECESSARY PACKAGES

ID_ = '2'                       #
ID_in = ID_ + '@#'              #
ID_out = '#@' + ID_             #GIVE DEVICE A NAME

#EXAMPLE IF ID IS 1 AND SENDING TO 2
#EXAMPLE *SENT* MESSAGE (RAW) : 2@#Hello hey how are ya'?#@1
#EXAMPLE *RECIEVED* MESSAGE (RAW) : 1@#Pretty good, yourself?#@2

#SET CONNECTION DETAILS - SAME AS MODULE WAS SET.  -- Port may need modified between devices
ser = serial.Serial(
        port='/dev/ttyUSB0',
        baudrate=9600,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS,
        timeout=1
)

startTime = time.time() #If wanted, start a timer to log when things happen
while True:
                x=ser.readline()
                forMe = x.startswith(ID_in.encode())
                fromMe = x.endswith(ID_out.encode())
                if b"@#" not in x or fromMe == True:  #If it is from me or is not a message at all, do nothing
                        time.sleep(1/100)
                elif forMe == False and fromMe == False:  #If it is neither for nor from me, send a signal relaying the message
                        ser.write(x)
                        time.sleep(1/100)
                else:                            #If it is for me....
                        now = (time.time()-startTime)   #log the time
                        try:                                      #If it isnt a response msg (AKA IT HAS A SENDER TAG ON THE END)
                            sender=(x.split(b'#@')[1]).decode('utf-8')      #Identify sender so that they can be addressed back
                            sendTo=(sender.encode() + b'@#').decode("utf-8")        #Set up the part of the return message so it will be recieved
                            message=x.split(b"@#")[1]       #strip message of your ID
                            message=(message.split(b"#@")[0]).decode("utf-8")          #strip message of sender's ID
                            print(round(now,3), " - From Device " + sender + " : " + message)       #print message in a clean format
                            recvd = "To Device " + ID_ + " : " + message       #Main body of return message
                            ser.write(str.encode(sendTo + recvd))          #write out the return message - NO ID_out on reply message.
                            time.sleep(1)     #wait for a second so the message can be properly sent before resuming looking for messages
                        except:             #IF IT IS A RESPONSE ( NO SENDER TAG )
                            message=(x.split(b'@#')[1]).decode('utf-8') #Simply print the response with no sender ID
                            print(round(now,3), " - " + message)